FROM ubuntu:20.04
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && \
  apt-get install -y \
  openjdk-8-jre \
  curl \
  wget \
  fonts-liberation \
  libgbm1 \
  libgtk-3-0 \
  libxkbcommon0 \
  xdg-utils   

#  && \
#  apt-get clean && \
#  rm -rf /var/lib/apt/lists/*

#  xvfb \
#  supervisor \
#  netcat-traditional \
#  firefox \
#  ffmpeg \
#  curl \
#  gnupg \


RUN curl -s -L https://deb.nodesource.com/setup_10.x | bash - && \
  apt-get update && \
  apt-get install -y nodejs && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# RUN curl -LO https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
#   apt-get -f install -y ./google-chrome-stable_current_amd64.deb && \
#   rm google-chrome-stable_current_amd64.deb

# Add a non-privileged user for running Protrator
# RUN adduser --home /project --uid 1000 \
#  --disabled-login --disabled-password --gecos node node

# Add main configuration file
# ADD supervisord.conf /etc/supervisor/supervisor.conf

# Add service defintions for Xvfb, Selenium and Protractor runner
# ADD supervisord/*.conf /etc/supervisor/conf.d/

# By default, tests in /data directory will be executed once and then the container
# will quit. When MANUAL envorinment variable is set when starting the container,
# tests will NOT be executed and Xvfb and Selenium will keep running.
# ADD bin/run-protractor /usr/local/bin/run-protractor
# ADD bin/run-webdriver /usr/local/bin/run-webdriver

# Container's entry point, executing supervisord in the foreground
#CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/supervisor.conf"]

# Protractor test project needs to be mounted at /project
